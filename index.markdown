---
layout: default
title: Development Environments Documentation
---

# Development environments exam project

## Short overview
This project is a Dockerized verion of a bank app build with Django. It has been containerized, tested and run trough CI/CD pipelines.

## How to install and run the project

> NOTE: You will need an authenticator app to log in to the bank!

1. Clone the project from this repository and navigate to the folder. 
2. Make sure the Docker Desptop app is running
3. Run the command *RTE=dev docker compose up* to start the live server.
4. Navigate to http://127.0.0.1:8000/ in your brower
5. Log in as an employee with username *employee* and password *employee1234* to create a customer account.
6. Now you're all set!

## How to use the project
This project is a bank application where new customers can only be added by an employee. Customers can transfer money to their own or others accounts. Customers ranked silver or higher can take a loan. The rank of the customer is set by the employee.

## Test the project
In order to test the project, run the following command *RTE=test docker compose up*

### All rights and credits to Linea, Niklas and Signe from KEA
